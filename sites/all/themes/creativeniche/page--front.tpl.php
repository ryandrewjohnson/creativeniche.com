<?php print render($page['content']['metatags']); ?>
<!-- start:container -->
<div id="container" class="container">

    <!-- start:header -->
    <header class="clearfix">
        <p class="intro">Brief introductory text – lorem ipsum dolour sit amet.</p>
        <div class="column column--first">
            <h1 class="column__heading">I’M LOOKING FOR <span>TALENT.</span></h1>
            <a href="#" class="column__link"><h2>Search for Talent</h2></a>
            <a href="#" class="column__link"><h2>Inquire About How</br>to Hire Someone</h2></a>
        </div>
        <div class="column front">
            <h1 class="column__heading">I’M LOOKING FOR <span>WORK.</span></h1>
            <a href="#" class="column__link"><h2>Search Job Listings</h2></a>
            <a href="#" class="column__link"><h2>Register<br/>in Our System</h2></a>
        </div>
    </header>
    <!-- end:header -->
    
    <!-- start:content -->
    <div class="container-row clearfix">
        <div id="talent-container" class="main-container main-container--half">
            <div class="main-container__heading main-container__heading--no-divider">
                <h1>Featured Talent</h1>
                <nav class="flex-controls-contianer"></nav>
                <a href="#" class="main-container__icon"><span class='symbol'>&#xe071;<img class="font-replace" width="23" height="22" src="<?php echo base_path().path_to_theme();?>/images/icon-rss.png" /></span></a>
            </div>
            <div class="flexslider">
                <a href="#" class="flex-control-next">Next</a>
                <a href="#" class="flex-control-prev">Previous</a>
                <ul class="slides">
                    <li class="flex-slide">
                        <a class="flex-slide__heading">SG, Cincinnati Marketing Director</a>
                        <p class="flex-slide__body">SG is a seasoned, award-winning Marketing Director who works on content marketing strategy, web copy, brand strategy…<a href="#" class="flex-slide__cta">READ MORE</a></p>
                    </li>
                    <li class="flex-slide">
                        <a class="flex-slide__heading">SG, Cincinnati Marketing Director</a>
                        <p class="flex-slide__body">SG is a seasoned, award-winning Marketing Director who works on content marketing strategy, web copy, brand strategy…<a href="#" class="flex-slide__cta">READ MORE</a></p>
                    </li>
                    <li class="flex-slide">
                        <a class="flex-slide__heading">SG, Cincinnati Marketing Director</a>
                        <p class="flex-slide__body">SG is a seasoned, award-winning Marketing Director who works on content marketing strategy, web copy, brand strategy…<a href="#" class="flex-slide__cta">READ MORE</a></p>
                    </li>
                </ul>
            </div>
            <a href="#" class="main-container__cta">See More Jobs</a>
        </div>
        <div id="jobs-container" class="main-container main-container--half">
            <div class="main-container__heading main-container__heading--no-divider">
                <h1>Featured Jobs</h1>
                <nav class="flex-controls-contianer"></nav>
                <a href="#" class="main-container__icon"><span class='symbol'>&#xe071;<img class="font-replace" width="23" height="22" src="<?php echo base_path().path_to_theme();?>/images/icon-rss.png" /></span></a>
            </div>
            <div class="flexslider">
                <a href="#" class="flex-control-next">Next</a>
                <a href="#" class="flex-control-prev">Previous</a>
                <ul class="slides">
                    <li class="flex-slide">
                        <a class="flex-slide__heading">SG, Cincinnati Marketing Director</a>
                        <p class="flex-slide__body">SG is a seasoned, award-winning Marketing Director who works on content marketing strategy, web copy, brand strategy…<a href="#" class="flex-slide__cta">READ MORE</a></p>
                    </li>
                    <li class="flex-slide">
                        <a class="flex-slide__heading">SG, Cincinnati Marketing Director</a>
                        <p class="flex-slide__body">SG is a seasoned, award-winning Marketing Director who works on content marketing strategy, web copy, brand strategy…<a href="#" class="flex-slide__cta">READ MORE</a></p>
                    </li>
                    <li class="flex-slide">
                        <a class="flex-slide__heading">SG, Cincinnati Marketing Director</a>
                        <p class="flex-slide__body">SG is a seasoned, award-winning Marketing Director who works on content marketing strategy, web copy, brand strategy…<a href="#" class="flex-slide__cta">READ MORE</a></p>
                    </li>
                </ul>
            </div>
            <a href="#" class="main-container__cta">See More Talent</a>
        </div>    
    </div> <!--/container-row-->


    <div class="main-container main-container--tabbed">
        <div class="main-container__heading">
            <h1>What's New</h1>
            <nav class="main-container__subnav clearfix">
                <a href="#resources" class="subnav-bar__link subnav-bar__link--selected">Resources<span class="chevron"></span></a>
                <a href="#news" class="subnav-bar__link">News<span class="chevron"></span></a>
                <a href="#events" class="subnav-bar__link">Events<span class="chevron"></span></a>
                <a href="#twitter" class="subnav-bar__link">Twitter<span class="chevron"></span></a>
            </nav>
        </div>
        <div id="resources" class="tab-section tab-section--selected">
            <ul class="tile-container clearfix">
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">Here’s what really needs to be in your portfolio</a>
                    <p class="tile__body">A creative professional’s portfolio is a lot like a resume. It needs to convey a huge amount of...</p>
                    <a class="tile__cta">Read More</a>
                </li>
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">Here’s what really needs to be in your portfolio</a>
                    <p class="tile__body">A creative professional’s portfolio is a lot like a resume. It needs to convey a huge amount of...</p>
                    <a class="tile__cta">Read More</a>
                </li>
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">Here’s what really needs to be in your portfolio</a>
                    <p class="tile__body">A creative professional’s portfolio is a lot like a resume. It needs to convey a huge amount of...</p>
                    <a class="tile__cta">Read More</a>
                </li>
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">Here’s what really needs to be in your portfolio</a>
                    <p class="tile__body">A creative professional’s portfolio is a lot like a resume. It needs to convey a huge amount of...</p>
                    <a class="tile__cta">Read More</a>
                </li>
            </ul>
            <a href="#" class="main-container__cta"><span class='symbol'>&#xe071;<img class="font-replace" width="33" height="37" src="<?php echo base_path().path_to_theme();?>/images/icon-rss-lrg.png" /></span>Subscribe to the blog</a>
        </div> <!--/tab-section-->
        
        <div id="news" class="tab-section">
            <ul class="tile-container tile-container--tall clearfix">
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">Creative Niche bids farewell to President Stephen Hodges</a>
                    <p class="tile__body">A long-serving member of the Creative Niche team is moving on. President Stephen Hodges—an eight-year Nicher...</p>
                    <a class="tile__cta">Read More</a>
                </li>
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">Creative Niche’s second Project Greenhouse Cincinnati reminds why client relationships matter</a>
                    <p class="tile__body">Creative Niche’s second Project Greenhouse Cincinnati reminds why client relationships matter...</p>
                    <a class="tile__cta">Read More</a>
                </li>
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">Creative Niche achieves position on PROFIT Magazine’s 25th annual ...</a>
                    <p class="tile__body">FOR IMMEDIATE RELEASE First time creative staffing, recruitment and workforce management earns spot on prestigious list TORONTO, June 3...</p>
                    <a class="tile__cta">Read More</a>
                </li>
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">ADC Announces Global Advisory Board Members</a>
                    <p class="tile__body">FOR IMMEDIATE RELEASE Twitter VP of Global Brand Strategy, President of the Berlin School of Creative Leadership and prominent members of the advertising...</p>
                    <a class="tile__cta">Read More</a>
                </li>
            </ul>
        </div> <!--/tab-section-->

        <div id="events" class="tab-section">
            <ul class="tile-container tile-container--tall clearfix">
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">Creative Niche bids farewell to President Stephen Hodges</a>
                    <p class="tile__body">A long-serving member of the Creative Niche team is moving on. President Stephen Hodges—an eight-year Nicher...</p>
                    <a class="tile__cta">Read More</a>
                </li>
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">Creative Niche’s second Project Greenhouse Cincinnati reminds why client relationships matter</a>
                    <p class="tile__body">Creative Niche’s second Project Greenhouse Cincinnati reminds why client relationships matter...</p>
                    <a class="tile__cta">Read More</a>
                </li>
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">Creative Niche achieves position on PROFIT Magazine’s 25th annual ...</a>
                    <p class="tile__body">FOR IMMEDIATE RELEASE First time creative staffing, recruitment and workforce management earns spot on prestigious list TORONTO, June 3...</p>
                    <a class="tile__cta">Read More</a>
                </li>
                <li class="tile">
                    <p class="tile__date">April 08, 2014</p>
                    <a class="tile__heading">ADC Announces Global Advisory Board Members</a>
                    <p class="tile__body">FOR IMMEDIATE RELEASE Twitter VP of Global Brand Strategy, President of the Berlin School of Creative Leadership and prominent members of the advertising...</p>
                    <a class="tile__cta">Read More</a>
                </li>
            </ul>
        </div> <!--/tab-section-->
        
        <div id="twitter" class="tab-section">
            <ul class="tile-container clearfix">
                <li class="tile">
                    <p class="tile__tweet">RT <a href="http://www.twitter.com/MonikaRecruits" target="_blank">@MonikaRecruits</a>: Metrics. Cringing yet?  Help creatives understand them: <a href="http://t.co/emgubiwbhm" target="_blank">http://t.co/emgubiwbhm</a></p>
                    <p class="tile__tweet tile__tweet--time"><span class='symbol'>&#xe086;</span>9 hours ago</p>
                </li>
                <li class="tile tile--twitter">
                    <p class="tile__tweet">RT <a href="http://www.twitter.com/MonikaRecruits" target="_blank">@MonikaRecruits</a>: Metrics. Cringing yet?  Help creatives understand them: <a href="http://t.co/emgubiwbhm" target="_blank">http://t.co/emgubiwbhm</a></p>
                    <p class="tile__tweet tile__tweet--time"><span class='symbol'>&#xe086;</span>9 hours ago</p>
                </li>
                <li class="tile tile--twitter">
                    <p class="tile__tweet">RT <a href="http://www.twitter.com/MonikaRecruits" target="_blank">@MonikaRecruits</a>: Metrics. Cringing yet?  Help creatives understand them: <a href="http://t.co/emgubiwbhm" target="_blank">http://t.co/emgubiwbhm</a></p>
                    <p class="tile__tweet tile__tweet--time"><span class='symbol'>&#xe086;</span>9 hours ago</p>
                </li>
                <li class="tile tile--twitter">
                    <p class="tile__tweet">RT <a href="http://www.twitter.com/MonikaRecruits" target="_blank">@MonikaRecruits</a>: Metrics. Cringing yet?  Help creatives understand them: <a href="http://t.co/emgubiwbhm" target="_blank">http://t.co/emgubiwbhm</a></p>
                    <p class="tile__tweet tile__tweet--time"><span class='symbol'>&#xe086;</span>9 hours ago</p>
                </li>
            </ul>
            <a href="#" class="main-container__cta"><span class='symbol'>&#xe086;<img class="font-replace" width="33" height="31" src="<?php echo base_path().path_to_theme();?>/images/icon-twitter-shadow.png" /></span>Follow Us For The Latest Job Openings</a>
        </div>  <!--/tab-section-->  
    </div> <!--/main-container-->

    <!-- end:content -->
    
    <?php print render($page['testimonial']); ?>
    
    <div class="main-container main-container--light">
        <div class="main-container__row clearfix">
            <h1>Our Partners:</h1>
            <div class="logo-row logo-row--first">
                <a href="#"><img class="logo-row__img" src="<?php echo base_path().path_to_theme();?>/images/logo-partners1.png" alt=""></a>
                <a href="#"><img class="logo-row__img" src="<?php echo base_path().path_to_theme();?>/images/logo-partners2.png" alt=""></a>
            </div>
        </div>
        <div class="main-container__row clearfix">
            <h1>Affiliations:</h1>
            <div class="logo-row logo-row--first">
                <a href="#"><img class="logo-row__img" src="<?php echo base_path().path_to_theme();?>/images/logo-affiliates1.png" alt=""></a>
                <a href="#"><img class="logo-row__img" src="<?php echo base_path().path_to_theme();?>/images/logo-affiliates2.png" alt=""></a>
                <a href="#"><img class="logo-row__img" src="<?php echo base_path().path_to_theme();?>/images/logo-affiliates3.png" alt=""></a>
            </div>
            <div class="logo-row">    
                <a href="#"><img class="logo-row__img" src="<?php echo base_path().path_to_theme();?>/images/logo-affiliates4.png" alt=""></a>
                <a href="#"><img class="logo-row__img" src="<?php echo base_path().path_to_theme();?>/images/logo-affiliates5.png" alt=""></a>
                <a href="#"><img class="logo-row__img" src="<?php echo base_path().path_to_theme();?>/images/logo-affiliates6.png" alt=""></a>
            </div>
        </div>
    </div>

</div>
<!-- end:container -->

<script type="text/javascript">
    
    /**
     * Slides functionality
     */
    var sharedOptions = {
        animation: "slide",
        slideshowSpeed: 7000,
        animationSpeed: 600,
        directionNav: false
    };

    var jobsOptions = $.extend({}, sharedOptions, {controlsContainer: "#jobs-container .flex-controls-contianer"});

    var talentOptions = $.extend({}, sharedOptions, {controlsContainer: "#talent-container .flex-controls-contianer"});

    //trigger second slide
    jQuery(document).ready(function() {
        
        var $jobsSlider = jQuery('#jobs-container .flexslider').flexslider(jobsOptions);

        var $talentSlider = jQuery('#talent-container .flexslider').flexslider(talentOptions);

        // init slider arrow controls for jobs
        $jobsSlider.find('.flex-control-next, .flex-control-prev').bind('click', function(e) {
            e.preventDefault();

            var direction = $(this).hasClass('flex-control-next') ? 'next' : 'prev';
            $jobsSlider.flexslider(direction);
        });

        // init slider arrow controls for talent
        $talentSlider.find('.flex-control-next, .flex-control-prev').bind('click', function(e) {
            e.preventDefault();

            var direction = $(this).hasClass('flex-control-next') ? 'next' : 'prev';
            $talentSlider.flexslider(direction);
        });
    });
    
    /**
     * Tabbed menu functionality
     */
    var $tabSections = $('.main-container--tabbed .tab-section'),
        $tabLinks    = $('.main-container--tabbed .subnav-bar__link');

    $('.main-container--tabbed .subnav-bar__link').bind('click', function (e) {
        e.preventDefault();

        $tabSections.removeClass('tab-section--selected');
        $tabLinks.removeClass('subnav-bar__link--selected');
        var tabId = $(this).attr('href');
        $('.main-container--tabbed').find(tabId).addClass('tab-section--selected');
        $(this).addClass('subnav-bar__link--selected');
    })
</script>


