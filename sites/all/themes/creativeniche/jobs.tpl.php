<!-- start:container -->
<div id="container" class="container">

    <!-- start:header -->
    <header class="header--short clearfix">
        <h1>Jobs</h1>
        <p class="header__body">Brief instructional copy letting users know the different ways to sign up with Creative Niche.<br/>Lorem ipsum dolor sit amet, consectetur adipiscing.</p>
        <a href="#" class="header__cta">Register with us</a>
    </header>
    <!-- end:header -->


    <!-- start:content -->
    
    <div class="main-container">
        <div class="main-container__heading main-container__heading--no-divider">
            <h1>Find a job:</h1>
        </div>
        <div class="main-container__content">
            <div class="main-container__filters main-container__filters--nodivider">
                <input class="form-input" type="text" placeholder="Enter keywords, e.g. Designer">
            </div>
            <div class="main-container__filters">
                <a href="#" class="main-container__toggle">Filter by location</a>
                <ul class="check-list clearfix">
                    <li class="check-list__item"><input type="checkbox"><label>Canada</label></li>
                    <li class="check-list__item"><input type="checkbox"><label>China</label></li>
                    <li class="check-list__item"><input type="checkbox"><label>Switzerland</label></li>
                    <li class="check-list__item"><input type="checkbox"><label>Albuquerque</label></li>
                    <li class="check-list__item"><input type="checkbox"><label>Norfolk</label></li>
                </ul>
            </div>
            <div class="main-container__filters">
                <a href="#" class="main-container__toggle">Filter by job type</a>
                <ul class="check-list clearfix">
                    <li class="check-list__item"><input type="checkbox"><label>Account Services</label></li>
                    <li class="check-list__item"><input type="checkbox"><label>Execute</label></li>
                    <li class="check-list__item"><input type="checkbox"><label>Content Development</label></li>
                    <li class="check-list__item"><input type="checkbox"><label>Marketing &amp; Communications</label></li>
                    <li class="check-list__item"><input type="checkbox"><label>Creative &amp; Production</label></li>
                    <li class="check-list__item"><input type="checkbox"><label>Social Media</label></li>
                    <li class="check-list__item"><input type="checkbox"><label>Digital &amp; Media</label></li>
                </ul>
            </div>
            <div class="main-container__filters">
                <a href="#" class="btn-primary">Search</a>
                <span class="spacer">or</span>
                <a href="#">Clear Form</a>
            </div>
        </div>
    </div> <!--/main-container-->

    <?php
    $node_url = url(drupal_get_path_alias("jobs"), array('absolute' => TRUE));
    ?>

    <!-- start:content -->
    <div class="main-container main-container--results">
        <h1>Recent Jobs</h1>
        <div class="pagination clearfix">
            <p class="pagination__summary">Jobs: 1-20 of 200</p>
            <div class="pagination__links">
                <a href="#" class="pagination__num pagination__num--selected">1</a>
                <a href="#" class="pagination__num">2</a>
                <a href="#" class="pagination__num">3</a>
                <a href="#" class="pagination__num">4</a>
                <a href="#" class="pagination__num">5</a>
                <span class="pagination__spacer">...</span>
                <a href="#" class="pagination__num">15</a>
                <a href="#" class="pagination__num">16</a>
                <a href="#" class="pagination__link">Next</a>
            </div>
        </div> <!--/pagination-->

        <ul class="result-item clearfix">
            <li class="result-item__col result-item__col--first clearfix">
                <h2 class="result-item__heading">
                    <a href="#" class="result-item__link">
                        <span class="result-item__num">9032</span>
                        <p class="result-item__title">Digital Producer – Downtown Toronto,<br/>3-Month Contract</p>
                    </a>
                </h2>
                <a href="#" class="result-item__cta">Apply</a>
            </li>
            <li class="result-item__col result-item__col--last">
                <ul class="link-list">
                    <li class="link-list__item"><a href="#" class="link-list__link" onclick="" rel="sidebar" title="Associate Creative Director">+ Add to favorites</a></li>
                    <li class="link-list__item"><a class="addthis_button link-list__link">+ Share</a></li>
                </ul>
            </li>
        </ul> <!--/result-item-->

        <ul class="result-item clearfix">
            <li class="result-item__col result-item__col--first clearfix">
                <h2 class="result-item__heading">
                    <a href="#" class="result-item__link">
                        <span class="result-item__num">9032</span>
                        <p class="result-item__title">Digital Producer – Downtown Toronto,<br/>3-Month Contract</p>
                    </a>
                </h2>
                <a href="#" class="result-item__cta">Apply</a>
            </li>
            <li class="result-item__col result-item__col--last">
                <ul class="link-list">
                    <li class="link-list__item"><a href="#" class="link-list__link" onclick="" rel="sidebar" title="Associate Creative Director">+ Add to favorites</a></li>
                    <li class="link-list__item"><a class="addthis_button link-list__link">+ Share</a></li>
                </ul>
            </li>
        </ul> <!--/result-item-->

        <ul class="result-item clearfix">
            <li class="result-item__col result-item__col--first clearfix">
                <h2 class="result-item__heading">
                    <a href="#" class="result-item__link">
                        <span class="result-item__num">9032</span>
                        <p class="result-item__title">Digital Producer – Downtown Toronto,<br/>3-Month Contract</p>
                    </a>
                </h2>
                <a href="#" class="result-item__cta">Apply</a>
            </li>
            <li class="result-item__col result-item__col--last">
                <ul class="link-list">
                    <li class="link-list__item"><a href="#" class="link-list__link" onclick="" rel="sidebar" title="Associate Creative Director">+ Add to favorites</a></li>
                    <li class="link-list__item"><a class="addthis_button link-list__link">+ Share</a></li>
                </ul>
            </li>
        </ul> <!--/result-item-->

        <ul class="result-item clearfix">
            <li class="result-item__col result-item__col--first clearfix">
                <h2 class="result-item__heading">
                    <a href="#" class="result-item__link">
                        <span class="result-item__num">9032</span>
                        <p class="result-item__title">Digital Producer – Downtown Toronto,<br/>3-Month Contract</p>
                    </a>
                </h2>
                <a href="#" class="result-item__cta">Apply</a>
            </li>
            <li class="result-item__col result-item__col--last">
                <ul class="link-list">
                    <li class="link-list__item"><a href="#" class="link-list__link" onclick="" rel="sidebar" title="Associate Creative Director">+ Add to favorites</a></li>
                    <li class="link-list__item"><a class="addthis_button link-list__link">+ Share</a></li>
                </ul>
            </li>
        </ul> <!--/result-item-->

        <div class="pagination clearfix">
            <p class="pagination__summary">Jobs: 1-20 of 200</p>
            <div class="pagination__links">
                <a href="#" class="pagination__num pagination__num--selected">1</a>
                <a href="#" class="pagination__num">2</a>
                <a href="#" class="pagination__num">3</a>
                <a href="#" class="pagination__num">4</a>
                <a href="#" class="pagination__num">5</a>
                <span class="pagination__spacer">...</span>
                <a href="#" class="pagination__num">15</a>
                <a href="#" class="pagination__num">16</a>
                <a href="#" class="pagination__link">Next</a>
            </div>
        </div> <!--/pagination-->

    </div> <!--/main-container-->

    <!-- end:content -->

</div><!-- end:container -->


<script>
    $(document).ready(function () {
        $('.main-container__toggle').bind('click', function (e) {
            e.preventDefault();

            $(this).parents('.main-container__filters').toggleClass('main-container__filters--expanded');
            $(this).next('.check-list').slideToggle();
        });
    });
</script>