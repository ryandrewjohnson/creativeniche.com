<?php
// $Id: html.tpl.php,v 1.6 2010/11/24 03:30:59 webchick Exp $

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?><!doctype html>
<!--[if IE 7]>         <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->

<head profile="<?php print $grddl_profile; ?>">
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
	
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    
    <?php //print $styles; ?>
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.com/cssapi/9d5a579f-c9d1-41b6-aef3-d3cfadeb0ea5.css"/>
    

    <link rel="stylesheet" href="<?php echo base_path().path_to_theme();?>/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_path().path_to_theme();?>/css/reset.css">
    <link rel="stylesheet" href="<?php echo base_path().path_to_theme();?>/css/main.css">
    <link rel="stylesheet" href="<?php echo base_path().path_to_theme();?>/css/style.css">

	
    <link rel="stylesheet" href="<?php echo base_path().path_to_theme();?>/css/ipad.css" media="only screen and (min-width: 768px) and (max-width: 1024px)">
	<link rel="stylesheet" href="<?php echo base_path().path_to_theme();?>/css/iphone.css" media="only screen and (max-device-width: 480px)">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    
    <?php print $scripts; ?>

    <script src="<?php echo base_path().path_to_theme();?>/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_path().path_to_theme();?>/js/placeholders.min.js"></script>
    
    <script type="text/javascript">var switchTo5x=true;</script>
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher:'912862ce-3aa2-442d-a3dc-767c99e2fd44'});</script>
    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4f4cd69d04eac5b2"></script>

	<!--[if IE 7]>
        <script src="<?php echo base_path().path_to_theme();?>/js/ie.js"></script>
	<![endif]-->

	<!--[if lt IE 9]>
        <link rel="stylesheet" href="<?php echo base_path().path_to_theme();?>/css/ie.css">
	<![endif]-->

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
  

  <?php
  /*only for admin logged*/
  if($is_admin){
  ?>
    <link rel="stylesheet" href="<?php echo base_path().path_to_theme();?>/css/admin.css" type="text/css" />
  <?php
  }
  ?>
</head>
<body>
    
<?php print $page_top; ?>
    
<!-- start:top -->
<div id="top" class="clearfix">

    <!-- start:white -->
    <div class="white shadow">
        <div class="container clearfix">

            <!-- start:logo -->
            <a href="/" class="logo" title="Creative Niche Inc.">Creative Niche Inc.</a>
            <p class="contact-line">1.855.360.3893 in Canada/USA</p>
            <!-- end:logo -->

            <!-- start:right -->
            <div class="right clearfix">
                <div class="search">
                    <form method="get" action="/search_results">
                        <input type="text" name="s_value" class="text">
                        <input type="submit" class="submit" value="">
                    </form>
                </div>
                <nav class="social">
                    <a href="<?php echo $twitter_url;?>" class="social__icon social__icon--twitter"></a>
                    <a href="<?php echo $facebook_url;?>" class="social__icon social__icon--facebook"></a>
                    <a href="<?php echo $linked_in_url;?>" class="social__icon social__icon--linkedin"></a>
                    <a href="<?php echo $youtube_url;?>" class="social__icon social__icon--youtube"></a>
                </nav>
            </div>
            <!-- end:right -->

        </div>
    </div>
    <!-- end:white -->

    <!-- start:gray -->
    <div class="gray shadow">
        <div class="container clearfix">
            <!-- start:nav -->
            <?php print render($main_navigation_region); ?>
            <!-- end:nav -->

            <ul class="links clearfix">
                <li><a href="/about">About</a></li>
                <li><a href="/team">Team</a></li>
                <li><a href="/contact">Contact</a></li>
            </ul>
        </div>
    </div>
    <!-- end:gray -->

    <!-- start:dropdowns -->
    <div class="dropdowns main-dropdowns container">
        <div class="dropdown business">
            <ul><?php print render($business_menu); ?></ul>
        </div>

        <div class="dropdown talent">
            <ul><?php print render($talent_menu); ?></ul>
        </div>
    </div>
    <!-- end:dropdowns -->

</div>
<!-- end:top -->


<?php print $page; ?>
<?php print $page_bottom; ?>
    
<!-- start:footer -->
<footer>
		<!-- start:gray -->
		<div class="gray shadow">
			<div class="container">
				<!-- start:footer toggle -->
				<a href="#" class="footer-toggle"><span></span></a>
				<!-- end:footer toggle -->
				<!-- start:footer expand -->
				<div class="footer-expand clearfix">
                    <?php print render($foot_office); ?>
                    <?php print render($affiliations_partners); ?>
                </div>
			</div>
		</div>
		<!-- end:gray -->

		<!-- start:white -->
		<div class="white shadow">
			<div class="container clearfix">
                <!-- start:left -->
                <div class="left">
                    <section class="connect">
                        <h5><?php echo $lets_connect;?></h5> <?php echo $lets_connect_suffix;?>
                    </section>
                    <ul class="links clearfix">
                        <li><a href="/privacy_policy">Privacy policy</a></li>
                        <li><a href="/contact">Feedback</a></li>
                        <!--<li><a href="/press_center">Press center</a></li>-->
                        <li><a href="/">Sitemap</a></li>
                    </ul>
                </div>
                <!-- end:left -->
                <!-- start:right -->
                <div class="right clearfix">
                    <nav class="social">
                        <a href="<?php echo $twitter_url;?>" class="social__icon social__icon--twitter"></a>
                        <a href="<?php echo $facebook_url;?>" class="social__icon social__icon--facebook"></a>
                        <a href="<?php echo $linked_in_url;?>" class="social__icon social__icon--linkedin"></a>
                        <a href="<?php echo $youtube_url;?>" class="social__icon social__icon--youtube"></a>
                    </nav>
                </div><!-- end:right -->
            </div>
		</div><!-- end:white -->
</footer><!-- end:footer -->


<!-- Start of Async HubSpot Analytics Code -->
<script type="text/javascript">
	(function(d,s,i,r) {
		if (d.getElementById(i)){return;}
		var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
		n.id=i;n.src='//js.hubspot.com/analytics/'+(Math.ceil(new Date()/r)*r)+'/181804.js';
		e.parentNode.insertBefore(n, e);
	})(document,"script","hs-analytics",300000);
</script>
<!-- End of Async HubSpot Analytics Code -->

<!-- LeadFormix -->
<a href="https://www.leadformix.com" title="Marketing Automation" onclick="window.open(this.href);return(false);">
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://vlog.leadformix.com/" : "https://vlog.leadformix.com/");
<!--
bf_action_name = '';
bf_idsite = 8798;
bf_url = pkBaseURL+'bf/bf.php';
(function() {
    var lfh = document.createElement('script'); lfh.type = 'text/javascript'; lfh.async = true;
    lfh.src = pkBaseURL+'bf/lfx.js';
    var s = document.getElementsByTagName('head')[0]; s.appendChild(lfh);
  })();
//-->
</script>
<noscript><p>Marketing Automation Platform <img src="https://vlog.leadformix.com/bf/bf.php" style="border:0" alt="Marketing Automation Tool"/></p>
</noscript>
</a>
<!-- /LeadFormix -->

<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19719199-1']);
  _gaq.push(['_setDomainName', 'creativeniche.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>

<script type="text/javascript">
var _bsc = _bsc || {};
(function() {
    var bs = document.createElement('script');
    bs.type = 'text/javascript';
    bs.async = true;
    bs.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://d2so4705rl485y.cloudfront.net/widgets/tracker/tracker.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(bs, s);
})();
</script>


</body>
</html>